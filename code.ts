class MessageCache {
	constructor(public client: AethorClient) {}
	async get(id: string | bigint) {
		return await this.client.gateway.getMessage(id.toString());
	}
	async all() {
		return await this.client.gateway.getMessages();
	}
}

class Gateway extends EventEmitter {
	conn!: Deno.DatagramConn;
	client: AethorClient;
	constructor(client: AethorClient) {
		super();
		this.client = client;
	}
	async sendJson(json: any) {
		return await this.send(JSON.stringify(json));
	}
	async send(text: string) {
		const peerAddr: Deno.NetAddr = {
			transport: 'udp',
			hostname: '0.0.0.0',
			port: 3999,
		};
		await this.conn.send(new TextEncoder().encode(text), peerAddr);
	}
	async createcachePromise(
		id: string,
		type: string,
		guild?: string | undefined
	) {
		console.log(type, id);
		const obj: {
			rej: any;
			res: any;
			guild?: string;
			type: string;
		} = {
			rej: undefined,
			res: undefined,
			guild,
			type,
		};
		let r = new Promise((res, rej) => {
			obj.rej = rej;
			let id = setTimeout(() => {
				res(undefined);
				obj.res = () => undefined;
			}, 5);
			obj.res = (v: any) => {
				clearTimeout(id);
				res(v);
			};
			// obj.res = res;
		});
		this.client.cachePromises.set(id, obj);
		await this.sendJson({
			wants: type,
			id: id,
			guild,
		});
		return await r;
	}
	async getUser(id: string, guild: string) {
		return await this.createcachePromise(id, 'Member', guild);
	}
	async getChannel(id: string, guild: string) {
		return await this.createcachePromise(id, 'Channel', guild);
	}
	async getMessage(id: string) {
		return await this.createcachePromise(id, 'Message');
	}
	async getMessages() {
		return await this.createcachePromise('', 'Messages');
	}
	async getGuild(id: string) {
		return await this.createcachePromise(id, 'Guild');
	}
	async connect() {
		this.conn = Deno.listenDatagram({
			port: 4444,
			transport: 'udp',
			hostname: '0.0.0.0',
		});

		const N = 2048 * 16;
		const buff = new Uint8Array(N);
		cache.isReady = true;
		while (true) {
			const message = await this.conn.receive(buff);
			const msg = new TextDecoder().decode(message[0]);

			const parsed = JSON.parse(msg);
			if (parsed.data) {
				let res: any = {};
				if (parsed.data?.user_id) {
					res = {
						joinedAt: parsed.data.joined_at,
						premiumSince: parsed.data.premium_since,
						roles: parsed.data.roles,
						user: {
							...parsed.data,
							id: parsed.data.user_id,
						},
					};
				} else if (parsed.data?.guild_id) {
					res = {
						...parsed.data,
						id: parsed.data.guild_id,
					};
				} else {
					res = parsed.data;
				}
				const r = this.client.cachePromises.get(res.id);
				if (r?.type.endsWith('s')) {
					console.log(res);
				} else if (r) {
					//@ts-ignore -
					const struct = await structures[`createDiscordeno${r.type}`](
						res,
						r.guild
					);
					r.res(struct);
					this.client.cachePromises.delete(res.id);
				} else {
					console.log(`not found! ${res.id}`);
					this.client.cachePromises.forEach((x) => x.res(undefined));
					// this.client.cachePromises.forEach((x) => {
					//   x.rej();
					// });
					this.client.cachePromises.clear();
					// for (const [k, v] of this.client.cachePromises.entries()) {
					//   console.log({
					//     k,
					//     v,
					//   });
					//   v.rej();
					//   this.client.cachePromises.delete(k);
					// }
				}
			} else {
				await ws.handleOnMessage(parsed.payload, parsed.shard_id);
			}
		}
	}
}

class thing{
	async login() {
		    //@ts-ignore -
    cache.messages = new MessageCache(this);
    //@ts-ignore -
    cache.members = new UserCache(this);
    //@ts-ignore  -
    cache.channels = new ChannelCache(this);
    //@ts-ignore  -
    cache.guilds = new GuildCache(this);
		let botid = '826480286169956413';
		setBotId(botid);
		setApplicationId(botid);
		this.gateway = new Gateway(this);

		ws.handleOnMessage = async (message, shardId) => {
			// const shard = ws.shards.get(shardId);

			ws.log('RAW', { shardId, payload: message });

			eventHandlers.raw?.(message);
			await eventHandlers.dispatchRequirements?.(message, shardId);

			console.log(message.t);
			if (message.op !== DiscordGatewayOpcodes.Dispatch) return;
			if (!message.t) return;
			//@ts-ignore -
			// console.log(handlers[message.t]);
			//@ts-ignore -
			return handlers[message.t]?.(camelize(message), shardId);
		};

		await this.gateway.connect();
	}
}
