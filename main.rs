use dashmap::DashMap;
use futures_util::StreamExt;
use rmp_serde::Serializer;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use simd_json::{json, json_typed};
use std::error::Error;
use std::io;
use std::io::prelude::*;
use std::io::{BufRead, BufReader};
use std::net::SocketAddr;
use std::num::NonZeroU64;
use std::os::unix::net::{UnixListener, UnixStream};
use std::sync::Mutex;
use std::thread;
use std::{env, sync::Arc};
use tokio::io::Interest;
use tokio::net::{ToSocketAddrs, UdpSocket, UnixDatagram};
use twilight_cache_inmemory::iter::IterReference;
use twilight_cache_inmemory::{GuildResource, InMemoryCache, Reference};
use twilight_gateway::{
    cluster::{Cluster, ShardScheme},
    Event, EventTypeFlags, Intents,
};

struct Server {
    socket: Arc<UdpSocket>,
    buf: Vec<u8>,
    to_send: Option<(usize, SocketAddr)>,
    cache: Arc<InMemoryCache>,
}
#[derive(Serialize, Deserialize)]
struct Body {
    shard_id: u64,
    payload: Value,
}

#[derive(Serialize, Deserialize)]
struct Request {
    wants: String,
    id: String,
    guild: Option<String>,
}
#[derive(Serialize)]
struct CacheResponse<T> {
    data: T,
}

impl Server {
    async fn run(self) -> Result<Vec<u8>, Box<dyn Error + Send + Sync>> {
        let Server {
            socket,
            mut buf,
            mut to_send,
            cache,
        } = self;

        loop {
            println!("running loop!");
            if let Some((size, peer)) = to_send {
                println!("running loops!");
                println!("{:?}", peer);

                let query: Request = simd_json::from_slice(&mut buf[..size])?;
                let id = NonZeroU64::new(query.id.parse::<u64>().unwrap()).unwrap();
                let guild = query
                    .guild
                    .map(|x| NonZeroU64::new(x.parse::<u64>().unwrap()).unwrap());
                println!("Looking up: {}, guild {:?}, id: {}", query.wants, guild, id);
                println!("Looking up: {}, guild {:?}, id: {}", query.wants, guild, id);
                println!("Looking up: {}, guild {:?}, id: {}", query.wants, guild, id);
                let mut found = false;

                match query.wants.as_str() {
                    "Channel" => {
                        if let Some(data) = cache.guild_channel(id.into()) {
                            let res = encode(&CacheResponse {
                                data: &*data.value().resource(),
                            })?;
                            let ready = socket.ready(Interest::WRITABLE).await?;
                            println!("{:?}", data);

                            if ready.is_writable() {
                                socket.send_to(&res, "0.0.0.0:4444").await?;
                                found = true
                            }
                        }
                    }
                    "Member" => {
                        if let Some(channel) = cache.member(guild.unwrap().into(), id.into()) {
                            let res = encode(CacheResponse {
                                data: &*channel.value(),
                            })?;
                            let ready = socket.ready(Interest::WRITABLE).await?;

                            if ready.is_writable() {
                                socket.send_to(&res, "0.0.0.0:4444").await?;
                                found = true
                            }
                        }
                    }
                    "Guild" => {
                        if let Some(channel) = cache.guild(id.into()) {
                            let res = encode(CacheResponse {
                                data: &*channel.value(),
                            })?;
                            let ready = socket.ready(Interest::WRITABLE).await?;

                            if ready.is_writable() {
                                socket.send_to(&res, "0.0.0.0:4444").await?;
                                found = true
                            }
                        }
                    }
                    "Message" => {
                        if let Some(channel) = cache.message(id.into()) {
                            let res = encode(CacheResponse {
                                data: &*channel.value(),
                            })?;
                            let ready = socket.ready(Interest::WRITABLE).await?;

                            if ready.is_writable() {
                                socket.send_to(&res, "0.0.0.0:4444").await?;
                                found = true
                            }
                        }
                    }
                    "Channels" => {
                        let mut data = vec![];
                        for channel in cache.iter().guild_channels() {
                            data.push(channel);
                        }
                        let res = encode(
                            data.iter()
                                .map(|c| CacheResponse {
                                    data: &*c.value().resource(),
                                })
                                .collect::<Vec<_>>(),
                        )?;
                        let ready = socket.ready(Interest::WRITABLE).await?;

                        if ready.is_writable() {
                            socket.send_to(&res, "0.0.0.0:4444").await?;
                            found = true
                        }
                    }
                    "Guilds" => {
                        let mut data = vec![];
                        for channel in cache.iter().guilds() {
                            data.push(channel);
                        }
                        let res = encode(
                            data.iter()
                                .map(|c| CacheResponse { data: &*c.value() })
                                .collect::<Vec<_>>(),
                        )?;
                        let ready = socket.ready(Interest::WRITABLE).await?;

                        if ready.is_writable() {
                            socket.send_to(&res, "0.0.0.0:4444").await?;
                            found = true
                        }
                    }
                    "Messages" => {
                        let mut data = vec![];
                        for channel in cache.iter().messages() {
                            data.push(channel);
                        }
                        let res = encode(
                            data.iter()
                                .map(|c| CacheResponse { data: &*c.value() })
                                .collect::<Vec<_>>(),
                        )?;
                        let ready = socket.ready(Interest::WRITABLE).await?;

                        if ready.is_writable() {
                            socket.send_to(&res, "0.0.0.0:4444").await?;
                            found = true
                        }
                    }
                    "Members" => {
                        let mut data = vec![];
                        for channel in cache.iter().members() {
                            data.push(channel);
                        }
                        let res = encode(
                            data.iter()
                                .map(|c| CacheResponse { data: &*c.value() })
                                .collect::<Vec<_>>(),
                        )?;
                        let ready = socket.ready(Interest::WRITABLE).await?;

                        if ready.is_writable() {
                            socket.send_to(&res, "0.0.0.0:4444").await?;
                            found = true
                        }
                    }
                    _ => {
                        println!("Not found");
                    }
                }
                if !found {
                    println!("Not found!");
                    let res = encode(CacheResponse {
                        data: serde_json::json!({
                            "id": ""
                        }),
                    })?;
                    socket.send_to(&res, "0.0.0.0:4444").await?;
                }

                // let amt = socket.send_to(&buf[..size], &peer).await?;
            }

            // If we're here then `to_send` is `None`, so we take a look for the
            // next message we're going to echo back.
            println!("running loops!");
            to_send = Some(socket.recv_from(&mut buf).await?);
        }
    }
}

fn encode<T: Serialize>(mut val: T) -> Result<Vec<u8>, Box<dyn Error + Send + Sync>> {
    let buf = simd_json::to_vec(&val)?;
    Ok(buf)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    // Initialize the tracing subscriber.
    tracing_subscriber::fmt::init();

    // This is also the default.
    let scheme = ShardScheme::Auto;

    let intents = Intents::all();
    let (cluster, mut events) = Cluster::builder(env::var("DISCORD_TOKEN")?, intents)
        .shard_scheme(scheme)
        .event_types(EventTypeFlags::GUILDS | EventTypeFlags::SHARD_PAYLOAD)
        .build()
        .await?;
    let cluster = Arc::new(cluster);

    let cluster_spawn = Arc::clone(&cluster);

    tokio::spawn(async move {
        cluster_spawn.up().await;
    });

    let cache = Arc::new(InMemoryCache::builder().message_cache_size(10).build());

    let socket = Arc::new(UdpSocket::bind("0.0.0.0:3999").await?);

    let server = Server {
        socket: Arc::clone(&socket),
        buf: vec![0; 1024],
        to_send: None,
        cache: Arc::clone(&cache),
    };

    tokio::spawn(server.run());
    let socket_clone = Arc::clone(&socket);
    while let Some((id, event)) = events.next().await {
        cache.update(&event);
        match event {
            Event::ShardPayload(mut e) => {
                let ready = socket.ready(Interest::WRITABLE).await?;
                let text = String::from_utf8_lossy(&e.bytes);
                let payload: Value = simd_json::from_str(&mut text.to_string())?;

                let data = Body {
                    shard_id: id,
                    payload,
                };

                if ready.is_writable() {
                    let len = socket_clone.send_to(&encode(&data)?, "0.0.0.0:4444").await;
                    if let Err(e) = &len {
                        println!("{:#?}", e)
                    }
                    println!("\nSend data");
                    println!("Sent {:?} bytes\n", len);
                }
            }
            Event::MessageCreate(e) => {
                println!("{}", simd_json::to_string(&e).unwrap());
            }
            _ => {
                println!("Shard: {}, Event: {:?}", id, event.kind());
            }
        }
    }

    Ok(())
}
